# Binaries on my laptop used for work between 2015-18

Some of the command line tools are copied to this repository.
Some larger software is given with link.

All these files are installed at `~/bin` (roughly 50GB in size).

## Generic small shellscripts

*  [ack](https://beyondgrep.com/) and friends
*  activate-file   *see here*
*  deactivate-file  *see here*
*  disableScreenSaver.sh  *see here*
*  git-meld  *see here*
*  ipv6-disable-temporarily  *see here*
*  open   *see here*
*  pdf-align   *see here*
*  pdf-decrease-size   *see here*
*  pdf-generate-blank   *see here*
*  sprunge   *see here*

## GUI specific

*  `konsole-shell`  - blurred transparent shells on X11 for KDE/Konsole  *see here*

## Go and Gopass

*  go -> /usr/lib/go-1.9/bin/go
*  gofmt -> /usr/lib/go-1.9/bin/gofmt
*  gopass -> gopass.bin/bin/gopass
*  gopass.bin - https://www.gopass.pw/

## HDFView

*  hdfview -> HDFView-2.11/bin/hdfview.sh
*  HDFView-2.11  - https://www.hdfgroup.org/downloads/hdfview/
*  HDFView-2.11.tar.gz

## Other software

*  intel_studio 
*  [JDownloader](http://jdownloader.org/)
*  jetbrains
*  [SageMath](http://www.sagemath.org/)
*  teamviewer
*  [mc](https://github.com/minio/mc) - Minio Client for cloud storage and filesystems

*  Intel Compilers; `loadintel`

## IDEs

*  eclipse/cpp-neon
*  [KDevelop](https://www.kdevelop.org/) -> KDevelop-5.0.3-x86_64.AppImage
*  KDevelop-5.0.3-x86_64.AppImage

## Maple (using a LRZ license)

*  maple12-64
*  maple12-64.tar.gz
*  maple2015
*  maple2015.tar.gz
*  maple-ssh

## Mathematica (using a LRZ license)

*  math -> mathematica9.0/Executables/math
*  mathematica -> mathematica11.1.1/Executables/mathematica
*  Mathematica -> mathematica11.1.1/Executables/Mathematica
*  mathematica11.1.1
*  Mathematica11-1-1.tar.gz
*  mathematica9.0
*  MathKernel -> mathematica9.0/Executables/MathKernel
*  math-ssh
*  mcc -> mathematica9.0/Executables/mcc

## Matlab (using an institutes license)

*  matlabR2015b-64
*  matlab-ssh
*  matlab.tar.gz

## ParaView (open source 3D/VTK viewer)

*  paraview5.3 - https://www.paraview.org/
*  ParaView-5.3.0-Qt5-OpenGL2-MPI-Linux-64bit
*  ParaView-5.3.0-Qt5-OpenGL2-MPI-Linux-64bit.tar.gz
*  ParaView-5.3.0-RC1-Qt5-OpenGL2-MPI-Linux-64bit.tar.gz

## Handy tools for EinsteinToolkit/Cactus/ExaHyPE

*  pardiff -> /home/sven/numrel/ET/pycactuset/PostCactus/bin/pardiff  - PostCactus, included in [PyCactusET](https://bitbucket.org/DrWhat/pycactuset/)
*  parprint -> /home/sven/numrel/ET/pycactuset/PostCactus/bin/parprint
*  exa -> /home/sven/numrel/exahype/Engine-ExaHyPE/Miscellaneous/BuildScripts/exa.sh - [ExaHyPE](https://gitlab.lrz.de/exahype/ExaHyPE-Engine)
*  exaplayer -> /home/sven/numrel/exahype/master/Code/Miscellaneous/Postprocessing/exaplayer.py
*  exareader -> /home/sven/numrel/exahype/master/Code/Miscellaneous/Postprocessing/exareader.py
*  exa-toolkit2 -> /home/sven/numrel/exahype/Toolkit2/Miscellaneous/BuildScripts/relocatable-exa.sh
*  [mexa](https://bitbucket.org/svek/mexa) -> /home/sven/numrel/exahype/Engine-ExaHyPE/Miscellaneous/MetaSpecfile/mexa.py
*  [pygraph](https://bitbucket.org/dradice/pygraph) -> /home/sven/.local/bin/pygraph
*  [rug](https://bitbucket.org/svek/rugutils) -> /home/sven/numrel/ET/rugutils/rug
*  webmize  *also here*

## Tecplot (3D visualization tool, proprietary, with institutes Licene)

*  tecplot-2016r2
*  tecplot-2016r2.tar.gz
*  tecplot-2017r1
*  tecplot-2017r1.tar.gz

## Totalview debugger (with institute license)

*  totalview -> /home/sven/bin/totalview.bin/toolworks/totalview.2017.0.12/bin/tv8
*  totalview2016
*  totalview2016.bin
*  totalview.bin

## Visit (3D/VTK visualization, Open Source)

*  visit - https://wci.llnl.gov/
*  visit2_10_3.linux-x86_64
*  visit2.13
*  visit2_13_0.linux-x86_64
*  visit2_13_0.linux-x86_64-ubuntu14.tar.gz
*  visit2_13_0.linux-x86_64-ubuntu14-wmesa.tar.gz
*  visit2_13_0.linux-x86_64-wmesa
*  visit2_13_2.linux-x86_64
*  visit2_13_2.linux-x86_64-ubuntu14.tar.gz
